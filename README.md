# VimAI Navigation Client Example

This example project demonstrates the usage of VimAI Android SDK to implement AR-based Navigation app. Written entierly in Kotlin, uses [androidx](https://developer.android.com/jetpack) components.

It uses Sceneform engine for rendering, ARCore is necessary for the positioning to work. For the more in-detail instructions how to use the SDK, check the NavigationFragment.kt file. 
